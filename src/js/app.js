'use strict';

window.$ = window.jQuery = require('jquery/dist/jquery.js');

//require('bootstrap-sass/assets/javascripts/bootstrap/affix.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/alert.js');
require('bootstrap-sass/assets/javascripts/bootstrap/button.js');
require('bootstrap-sass/assets/javascripts/bootstrap/carousel.js');
require('bootstrap-sass/assets/javascripts/bootstrap/collapse.js');
require('bootstrap-sass/assets/javascripts/bootstrap/dropdown.js');
require('bootstrap-sass/assets/javascripts/bootstrap/modal.js');
require('bootstrap-sass/assets/javascripts/bootstrap/tooltip.js');
require('bootstrap-sass/assets/javascripts/bootstrap/popover.js');
//require('bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js');
require('bootstrap-sass/assets/javascripts/bootstrap/tab.js');
require('bootstrap-sass/assets/javascripts/bootstrap/transition.js');

require('slick-carousel/slick/slick.js');

require('jquery.inputmask/dist/jquery.inputmask.bundle.js');

$(document).on('ready', function () {
  $('[name="phone"]').inputmask("(99)9999-9999[9]");
  
  $('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(120);
  }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(120);
  });
  
  // Localizacao
  
  // TODO implementar funcionalidade de localidade
//  var locationBtn = $('.btn-location');
//  locationBtn.popover();
//  
//  if ('geolocation' in navigator) {
//    var findUserState = function () {
//      window.populateState = function (data) {
//        var state = data.address.state.toLowerCase();
//
//        console.log(state);
//      }
//
//      navigator.geolocation.getCurrentPosition(function (position) {
//        $.ajax({
//          url: 'http://nominatim.openstreetmap.org/reverse',
//          data: {
//            lat: position.coords.latitude,
//            lon: position.coords.longitude,
//            format: 'json',
//            json_callback: 'populateState'
//          },
//          dataType: 'jsonp'
//        }).done(function (scriptUrl) {
//          $('body').appendChild('<script src="' + scriptUrl + '">');
//        });//.fail(tratamento em erros na requisicao);
//      });
//    };
//
//    findUserState();
//  } else {
//    locationBtn.popover('show');
//  }
  
  // Clique accordion FOOTER.
  $('.accordion-click').click(function(){
    var marca = $(this).attr('data-maker');
    if(!$(this).hasClass('aberto')){
      $(".accordion-click").removeClass('aberto');
      $(this).addClass('aberto');
      $(".store-address .tab-pane").slideUp();
      $("#" + marca).slideDown();
    }else{
      $("#" + marca).slideUp();
      $(this).removeClass('aberto');
    }
  });
  
  
  // Clique accordion OFICINA.
  $('.accordion-item a').click(function(){
    var tipo = $(this).attr('data-type');
    if(!$(this).hasClass('aberto')){
      $(".accordion-item a").removeClass('aberto');
      $(this).addClass('aberto');
      $(".accordion-content").slideUp();
      $("#" + tipo).slideDown();
    }else{
      $("#" + tipo).slideUp();
      $(this).removeClass('aberto');
    }
  });
  
  // Bootstrap accordion init
  var accordions  = $('.panel-group'),
      activeClass = 'panel-active';
  
  $(accordions).find(' > .panel').on('show.bs.collapse', function (e) {
    accordions.find('.panel-heading').removeClass(activeClass);
    $(this).find('.panel-heading').addClass(activeClass);
  });

  // Bootstrap tabs init
  $('.nav-tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  // Yamm init
  $('.yamm .dropdown-menu').on('click', function (e) {
    e.stopPropagation();
  });
  
  // Slick carousel init
  var defaultSlick = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  // Slick carousel init
  var conceptualBanner = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  var btnArrow = function (side) {
    return '<button><i class="rw rw-gray rw-chevron-' + (side == 1 ? "right" : "left") + '"></i></button>';
  };

  var slickCarConfig = {
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    slidesToShow: 5,
    nextArrow: btnArrow(1),
    prevArrow: btnArrow(0),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  
  var btnArrow2 = function (side) {
    return '<button><i class="rw rw-gray rw-boomerang-' + (side == 1 ? "right" : "left") + '"></i></button>';
  };
  
  $('.slick-detail-sync').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slick-car-detail'
  });
  
  var slickCarDetailConfig = {
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    slidesToShow: 5,
    nextArrow: btnArrow2(1),
    prevArrow: btnArrow2(0),
    asNavFor: '.slick-detail-sync',
    focusOnSelect: true,
    lazyLoad: 'ondemand',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  };

  var slickNewGalleryConfig = {
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    slidesToShow: 4,
    nextArrow: btnArrow2(1),
    prevArrow: btnArrow2(0),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  $('.slick-promo').slick(defaultSlick);
  $('.slick-car').slick(slickCarConfig);
  $('.slick-new-gallery').slick(slickNewGalleryConfig);
  $('.slick-car-detail').slick(slickCarDetailConfig);
  $('.slick-conceptual').slick(conceptualBanner);
  
  $('.new-car-colors > li a').on('click', function () {
    var imgSrc = $(this).data('sync');
    
    $('#new-car').attr('src', imgSrc);
  });
  
  $('.gallery-modal').on('show.bs.modal', function (e) {
    $('.modal-body', this).html($(e.relatedTarget).clone());
  });
  
  $('#car-carousel > li').on('click', function () {
    var activeCategory = $.trim($(this).html());

    $('.car-carousel-category').html(activeCategory);
  });
  

  var qtd_active = 3;
  $('.models-open input').click(function(){
    var target = $(this).attr('getModel');
    var active = $(this).attr('getActive');
    if ( active == 'nao' && qtd_active < 3){
      $('.'+target).show();
      qtd_active = qtd_active + 1;
      //$(this).addClass('ativado');
      $(this).attr('getActive' , 'sim');
    }
    if (active == 'sim'){	
      $('.'+target).hide();
      qtd_active = qtd_active - 1;
      //$(this).removeClass('ativado');
      $(this).attr('getActive' , 'nao');
    }
  });

  $('.models-open-mobile input').click(function(){
    var target = $(this).attr('getModel');
    var active = $(this).attr('getActive');
    if ( active == 'nao' && qtd_active < 3){
      $('.'+target).show();
      qtd_active = qtd_active + 1;
      //$(this).addClass('ativado');
      $(this).attr('getActive' , 'sim');
    }
    if (active == 'sim'){
      $('.'+target).hide();
      qtd_active = qtd_active - 1;
      //$(this).removeClass('ativado');
      $(this).attr('getActive' , 'nao');
    }
  });
  
  // Bloqueia mais de 3 escolhas nas informações do carros.
  $(".version-default label input").on("change", function(){
    if($(".version-default label :checked").length > 3)
      $(this).prop("checked", false);
  });

  // Bloqueia mais de 3 escolhas nas informações do carros no mobile.
  $(".version-mobile label input").on("change", function(){
    if($(".version-mobile label :checked").length > 3)
      $(this).prop("checked", false);
  });
  
  var mapEl = $('#map');
  
  if (mapEl.length) {
    // Função de criação do MAPA.
    function setCoveredAreaGeolocation(latitude, longitude, id) {
      var map;
      var marker;
      var latlng = new google.maps.LatLng(latitude, longitude);

      var options = {
          zoom: 13,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          scrollwheel: false
      };
      //alert(id);
      map = new google.maps.Map(document.getElementById(id), options);

      marker = new google.maps.Marker({
          map: map,
          draggable: true,
      });

      marker.setPosition(latlng);
    }

    var latitude = mapEl.attr('data-lat');
    var longitude = mapEl.attr('data-long');    
    setCoveredAreaGeolocation(latitude, longitude, "map");
  }
  
});
